﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_3
{
    class InventoryManager
    {
        public List<string[]> inventoryList;
        public InventoryManager ()
        {
            inventoryList = new List<string[]>();
        }
        public void AddNewItem(String partNumber, String productName, double productWeight, String productColor, bool productFragile)
        {
            string[] newInventoryArray = new string[] { partNumber, productName, productWeight.ToString(), productColor, productFragile.ToString() };
            inventoryList.Add(newInventoryArray);
        }

        public void RemoveItem(String partNumber, int quantity)
        {
            while (quantity != 0)
            {
                int index = 0;
                foreach (string[] array in inventoryList)
                {
                    if (partNumber == array[0])
                    {
                        break;
                    }
                    else
                    {
                        index++;
                    }
                }
                inventoryList.RemoveAt(index);
                quantity--;
            }
        }

        public void RestockItem(String partNumber, String productName, double productWeight, String productColor, bool productFragile)
        {
            string[] newInventoryArray = new string[] { partNumber, productName, productWeight.ToString(), productColor, productFragile.ToString() };
            inventoryList.Add(newInventoryArray);
        }

        public List<string[]> DisplayAllItems()
        {
            return inventoryList;
        }

        public string[] SearchForItem(String partNumber)
        {
            int index = 0;
            foreach (string[] array in inventoryList)
            {
                if (partNumber == array[0])
                {
                    break;
                }
                else
                {
                    index++;
                }
            }
            return inventoryList[index];
        }
    }
}
