﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_3
{
    public partial class Form2 : Form
    {
        public List<string[]> inventoryList = new List<string[]>();

        public Form2()
        {
            InitializeComponent();
        }

        private void DisplayButton_Click(object sender, EventArgs e)
        {
            string textToDisplay = "";
            foreach (string[] stringArray in inventoryList)
            {
                string separator = ", ";
                textToDisplay += string.Join(separator, stringArray) + "\r\n";
                MessageBox.Show(stringArray.ToString());
            }
            displayAllBox.Text = textToDisplay;
        }
    }
}
