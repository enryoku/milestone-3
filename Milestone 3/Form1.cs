﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_3
{
    public partial class Form1 : Form
    {
        InventoryManager inventoryManagerReference = new InventoryManager();
        public bool productFragile = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            Form2 displayAllItems = new Form2();
            displayAllItems.inventoryList = inventoryManagerReference.inventoryList;
            this.Hide();
            displayAllItems.Show();

            displayAllItems.FormClosed += (sender2, e2) =>
            {
                this.Show();
                displayAllItems.inventoryList = inventoryManagerReference.inventoryList;
            };

            displayAllItems.VisibleChanged += (sender2, e2) =>
            {
                this.Show();
                displayAllItems.inventoryList = inventoryManagerReference.inventoryList;
            };
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            double productWeight = Convert.ToDouble(productWeightBox.Text);
            if (productFragileBox.Checked == true)
            {
                productFragile = true;
            }
            inventoryManagerReference.AddNewItem(partNumberBox.Text, productNameBox.Text, productWeight, productColorBox.Text, productFragile);
            MessageBox.Show("Item Added!");
            partNumberBox.Text = "";
            productNameBox.Text = "";
            productWeightBox.Text = "";
            productColorBox.Text = "";
            productFragileBox.Checked = false;
        }

        private void RemoveItemButton_Click(object sender, EventArgs e)
        {
            if (partNumberBox.Text == null)
            {
                MessageBox.Show("Please enter a valid part number!");
            }
            else
            {
                inventoryManagerReference.RemoveItem(partNumberBox.Text, 1);
            }
        }

        private void RestockItemButton_Click(object sender, EventArgs e)
        {
            double productWeight = Convert.ToDouble(productWeightBox.Text);
            if (productFragileBox.Checked == true)
            {
                productFragile = true;
            }
            inventoryManagerReference.RestockItem(partNumberBox.Text, productNameBox.Text, productWeight, productColorBox.Text, productFragile);
            MessageBox.Show("Item Added!");
            partNumberBox.Text = "";
            productNameBox.Text = "";
            productWeightBox.Text = "";
            productColorBox.Text = "";
            productFragileBox.Checked = false;
        }

        private void SearchForItemButton_Click(object sender, EventArgs e)
        {
            string[] foundItem = inventoryManagerReference.SearchForItem(partNumberBox.Text);
            string separator = ", ";
            searchItemBox.Text = string.Join(separator, foundItem);
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            partNumberBox.Text = "";
            productNameBox.Text = "";
            productWeightBox.Text = "";
            productColorBox.Text = "";
            productFragileBox.Checked = false;
        }
    }
}
