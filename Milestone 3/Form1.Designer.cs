﻿namespace Milestone_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.partNumberBox = new System.Windows.Forms.TextBox();
            this.productNameBox = new System.Windows.Forms.TextBox();
            this.productWeightBox = new System.Windows.Forms.TextBox();
            this.productColorBox = new System.Windows.Forms.TextBox();
            this.addNewItemButton = new System.Windows.Forms.Button();
            this.removeItemButton = new System.Windows.Forms.Button();
            this.restockItemButton = new System.Windows.Forms.Button();
            this.searchForItemButton = new System.Windows.Forms.Button();
            this.displayAllItemsButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.productFragileBox = new System.Windows.Forms.CheckBox();
            this.searchItemBox = new System.Windows.Forms.TextBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // partNumberBox
            // 
            this.partNumberBox.Location = new System.Drawing.Point(12, 12);
            this.partNumberBox.Name = "partNumberBox";
            this.partNumberBox.Size = new System.Drawing.Size(100, 20);
            this.partNumberBox.TabIndex = 0;
            // 
            // productNameBox
            // 
            this.productNameBox.Location = new System.Drawing.Point(118, 12);
            this.productNameBox.Name = "productNameBox";
            this.productNameBox.Size = new System.Drawing.Size(100, 20);
            this.productNameBox.TabIndex = 1;
            // 
            // productWeightBox
            // 
            this.productWeightBox.Location = new System.Drawing.Point(224, 12);
            this.productWeightBox.Name = "productWeightBox";
            this.productWeightBox.Size = new System.Drawing.Size(100, 20);
            this.productWeightBox.TabIndex = 2;
            // 
            // productColorBox
            // 
            this.productColorBox.Location = new System.Drawing.Point(330, 12);
            this.productColorBox.Name = "productColorBox";
            this.productColorBox.Size = new System.Drawing.Size(100, 20);
            this.productColorBox.TabIndex = 3;
            // 
            // addNewItemButton
            // 
            this.addNewItemButton.Location = new System.Drawing.Point(12, 142);
            this.addNewItemButton.Name = "addNewItemButton";
            this.addNewItemButton.Size = new System.Drawing.Size(100, 23);
            this.addNewItemButton.TabIndex = 5;
            this.addNewItemButton.Text = "Add New Item";
            this.addNewItemButton.UseVisualStyleBackColor = true;
            this.addNewItemButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // removeItemButton
            // 
            this.removeItemButton.Location = new System.Drawing.Point(118, 142);
            this.removeItemButton.Name = "removeItemButton";
            this.removeItemButton.Size = new System.Drawing.Size(100, 23);
            this.removeItemButton.TabIndex = 6;
            this.removeItemButton.Text = "Remove Item";
            this.removeItemButton.UseVisualStyleBackColor = true;
            this.removeItemButton.Click += new System.EventHandler(this.RemoveItemButton_Click);
            // 
            // restockItemButton
            // 
            this.restockItemButton.Location = new System.Drawing.Point(224, 142);
            this.restockItemButton.Name = "restockItemButton";
            this.restockItemButton.Size = new System.Drawing.Size(100, 23);
            this.restockItemButton.TabIndex = 7;
            this.restockItemButton.Text = "Restock Item";
            this.restockItemButton.UseVisualStyleBackColor = true;
            this.restockItemButton.Click += new System.EventHandler(this.RestockItemButton_Click);
            // 
            // searchForItemButton
            // 
            this.searchForItemButton.Location = new System.Drawing.Point(439, 142);
            this.searchForItemButton.Name = "searchForItemButton";
            this.searchForItemButton.Size = new System.Drawing.Size(100, 23);
            this.searchForItemButton.TabIndex = 8;
            this.searchForItemButton.Text = "Search for Item";
            this.searchForItemButton.UseVisualStyleBackColor = true;
            this.searchForItemButton.Click += new System.EventHandler(this.SearchForItemButton_Click);
            // 
            // displayAllItemsButton
            // 
            this.displayAllItemsButton.Location = new System.Drawing.Point(330, 142);
            this.displayAllItemsButton.Name = "displayAllItemsButton";
            this.displayAllItemsButton.Size = new System.Drawing.Size(100, 23);
            this.displayAllItemsButton.TabIndex = 9;
            this.displayAllItemsButton.Text = "Display All Items";
            this.displayAllItemsButton.UseVisualStyleBackColor = true;
            this.displayAllItemsButton.Click += new System.EventHandler(this.Button5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Part Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Product Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(221, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Product Weight";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(330, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Product Color";
            // 
            // productFragileBox
            // 
            this.productFragileBox.AutoSize = true;
            this.productFragileBox.Location = new System.Drawing.Point(439, 15);
            this.productFragileBox.Name = "productFragileBox";
            this.productFragileBox.Size = new System.Drawing.Size(97, 17);
            this.productFragileBox.TabIndex = 15;
            this.productFragileBox.Text = "Product Fragile";
            this.productFragileBox.UseVisualStyleBackColor = true;
            // 
            // searchItemBox
            // 
            this.searchItemBox.Location = new System.Drawing.Point(12, 51);
            this.searchItemBox.Name = "searchItemBox";
            this.searchItemBox.Size = new System.Drawing.Size(418, 20);
            this.searchItemBox.TabIndex = 16;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(15, 113);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(97, 23);
            this.clearButton.TabIndex = 17;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 177);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.searchItemBox);
            this.Controls.Add(this.productFragileBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.displayAllItemsButton);
            this.Controls.Add(this.searchForItemButton);
            this.Controls.Add(this.restockItemButton);
            this.Controls.Add(this.removeItemButton);
            this.Controls.Add(this.addNewItemButton);
            this.Controls.Add(this.productColorBox);
            this.Controls.Add(this.productWeightBox);
            this.Controls.Add(this.productNameBox);
            this.Controls.Add(this.partNumberBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox partNumberBox;
        private System.Windows.Forms.TextBox productNameBox;
        private System.Windows.Forms.TextBox productWeightBox;
        private System.Windows.Forms.TextBox productColorBox;
        private System.Windows.Forms.Button addNewItemButton;
        private System.Windows.Forms.Button removeItemButton;
        private System.Windows.Forms.Button restockItemButton;
        private System.Windows.Forms.Button searchForItemButton;
        private System.Windows.Forms.Button displayAllItemsButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox productFragileBox;
        private System.Windows.Forms.TextBox searchItemBox;
        private System.Windows.Forms.Button clearButton;
    }
}

